var _main = function () {
    var imgs_path = {
        background: 'img/background.png',
        land: 'img/land.png',
        pipe_down: 'img/pipe_down.png',
        pipe_up: 'img/pipe_up.png',

        bird0: 'img/bird0.png',
        bird1: 'img/bird1.png',
        bird2: 'img/bird2.png',

    }
    var game = Game.instance(imgs_path, function (g) {
        var s = new CoverScene(game)
        g.runWithScene(s)
    })
}

_main()
