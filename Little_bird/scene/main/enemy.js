class Enemy extends Element {
    constructor (game) {
        var type = randombetween(0, 2)
        var name = 'enemy' + type
        super(game, name)
        this.type = type

        this.setup()
    }

    setup() {
        var game = this.game
        this.speed = randombetween(1.5, 2) * (global_args.score / 100000 + 1)
        this.x = randombetween(0, game.canvas.width - this.w)
        this.y = -randombetween(246, 300)
        this.life = 1
        this.arm = this.type *2 - 1

        this.type = randombetween(0, 2)
        var name = 'enemy' + this.type
        this.texture = imgFromName(name)
    }
    update() {
        this.y += this.speed
        if (this.y > 600) {
            this.setup()
        }
        if (this.arm <= 0) {
            // 坠毁动画 TODO：需要找到更好的方式实现
            var down = new AnimationCanDie(this.game, this.scene.enemies_down_framenum[this.type], `enemy${this.type}_down`)
            down.x = this.x
            down.y = this.y

            var boom = new particleSystem(this.game, this.x + this.w / 2, this.y + this.h / 2, 10 * (this.type + 1))

            this.scene.elements.push(down)
            this.scene.elements.push(boom)
            this.setup()
        }
    }
}