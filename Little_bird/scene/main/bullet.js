class Bullet extends Element {
    constructor (game) {
        super(game, 'bullet')

        this.setup()
    }

    setup() {
        this.speed = config.bullet_speed
    }

    update() {
        this.y -= this.speed
        if (this.x < 0) {
            this.life = 0
        }
        for (e of this.scene.enemies) {
            this.actionWithEnemy(e)
        }
    }
    actionWithEnemy(e) {
        if (collide(this, e)) {
            this.life -= 1
            e.arm -= 1
            global_args.score += 100

            var boom = new particleSystem(this.game, this.x + this.w / 2, this.y + this.h / 2, 2)
            this.scene.elements.push(boom)

        }
    }
}