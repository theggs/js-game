class Bird extends AnimationMutiCon {
    constructor(game) {
        super(game, 3, 'bird')

        this.setup()
    }
    setup() {
        this.x = 100
        this.y = 200
        this.gy = 3
        this.vy = 0

        this.life = 1
    }
    update() {
        super.update()
        // 更新受力
        this.y += this.vy
        this.vy += this.gy * 0.2
        var h = this.game.scene.land.y
        if (this.y + this.h >= h) {
            this.y = h - this.h
        }
        // 更新角度
        if (this.rotation < 45) {
            this.rotation += 5
        }
        if (this.life <= 0) {
            this.game.actions = {}
        }
    }
    jump() {
        this.vy = -6
        this.rotation = -45    }
}