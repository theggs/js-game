class Backgound extends Element {
    constructor (game) {
        super(game, 'background')

        this.setup()
    }

    setup() {
        this.x = 0
        this.y = 0
    }
}