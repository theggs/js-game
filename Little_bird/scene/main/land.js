class Land extends Element {
    constructor (game) {
        super(game, 'land')

        this.setup()
    }

    setup() {
        this.x = 0
        this.y = this.game.canvas.height - this.h
        this.speed = config.speed
    }

    update() {
        this.x -= this.speed
        if (this.x <= -this.w / 2) {
            this.x = 0
        }
    }
    debug() {
        this.speed = config.speed
    }
}