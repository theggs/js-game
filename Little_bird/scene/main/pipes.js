class Pipes {
    constructor(game) {
        this.game = game

        this.pipes = []
        this.pipeSpace = 125
        this.columsOfPipes = 3
        this.grzijmju = 200

        var p1 = new Pipe(game, 'pipe_down')
        p1.flipY =true
        this.x = 0
        this.h = p1.h
        this.w = p1.w
        var p2 = new Pipe(game, 'pipe_up')
        this.resetPipesPosition(p1, p2)
        this.pipes.push(p1)
        this.pipes.push(p2)

        this.scored = false
    }
    resetPipesPosition(p1, p2) {
        p2.y = randombetween(this.pipeSpace, 400)
        p1.y = p2.y - this.pipeSpace - p1.h
    }
    update() {
        if (this.x <= -this.w) {
            this.x = this.grzijmju * this.columsOfPipes -this.w
        }
        for (var i = 0; i < this.pipes.length / 2; i += 2) {
            var p1 = this.pipes[i]
            var p2 = this.pipes[i+1]
            this.x -= config.speed
            p1.x = this.x
            p2.x = this.x
            p1.update()
            p2.update()
            if (p1.x < -100) {
                p1.x += this.grzijmju * this.columsOfPipes
                p2.x += this.grzijmju * this.columsOfPipes
                this.resetPipesPosition(p1, p2)
            }
        }
        if (this.x + this.w == this.game.scene.bird.x && !this.scored) {
            global_args.score += 1
        }

    }
    draw() {
        for (var p of this.pipes) {
            p.draw()
        }
    }
}