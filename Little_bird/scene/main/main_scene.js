class MainScene extends Scene {
    constructor(game) {
        super(game)

        this.setup()
        this.setupInputs()
    }
    setup() {
        var game = this.game

        global_args.score = 0
        config.speed = 2

        this.bg = new Backgound(game)
        this.land = new Land(game)
        this.bird = new Bird(game)

        this.pipes1 = new Pipes(game)
        this.pipes1.x = 500
        this.pipes2 = new Pipes(game)
        this.pipes2.x = 700
        this.pipes3 = new Pipes(game)
        this.pipes3.x = 900

        this.addElement(this.bg)
        this.addElement(this.pipes1)
        this.addElement(this.pipes2)
        this.addElement(this.pipes3)
        this.addElement(this.land)
        this.addElement(this.bird)

    }
    setupInputs() {
        var game = this.game
        // 注册按键事件
        game.registerAction('j', () => {
            this.bird.jump()
        })
    }
    update() {
        super.update()
        var h = this.land.y
        if (this.bird.y + this.bird.h >= h) {
            var s = new GameOverScene(this.game)
            this.game.replaceScene(s)
        }
    }
    draw() {
        super.draw()
        this.game.ctx.fillText(`分数: ${global_args.score}`, 25, 25)

    }
}