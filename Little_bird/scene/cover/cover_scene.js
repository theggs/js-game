class CoverScene extends Scene {
    constructor(game) {
        super(game)

        this.setup()
        this.setupInputs()
    }
    setup() {
        var game = this.game

        this.bg = new Backgound(game)
        this.land = new Land(game)
        this.bird = new CoverBird(game)

        this.addElement(this.bg)
        this.addElement(this.land)
        this.addElement(this.bird)
    }
    draw() {
        super.draw()
        // draw labels
        this.game.ctx.fillText('按 k 开始游戏', 50, 190)
    }
    setupInputs() {
        var game = this.game
        game.registerAction('k', function(){
            var s = new MainScene(game)
            game.replaceScene(s)
        })
    }
}