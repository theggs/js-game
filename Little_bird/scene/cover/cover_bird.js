class CoverBird extends Animation {
    constructor(game) {
        super(game, 3, 'bird')

        this.setup()
    }
    setup() {
        this.x = 100
        this.y = 200
    }
}