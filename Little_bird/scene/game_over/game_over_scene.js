class GameOverScene extends Scene {
    constructor(game) {
        super(game)
        game.registerAction('r', function(){
            var s = new CoverScene(game)
            game.replaceScene(s)
        })
    }
    draw() {
        // draw labels
        this.game.ctx.fillText('游戏结束, 按 r 返回标题界面', 50, 300)
        this.game.ctx.fillText(`分数：${global_args.score}`, 50, 350)
    }
}
