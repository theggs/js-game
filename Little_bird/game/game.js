class Game {
    constructor(imgs_path, runCallback) {
        this.imgs_path = imgs_path
        this.runCallback = runCallback
        this.keydowns =  {}
        this.actions = {}
        this.scene =  null

        this.canvas = e('#game-area')
        this.ctx = this.canvas.getContext('2d')
        var self = this
        // 监听按键
        window.addEventListener('keydown', event =>{
            this.keydowns[event.key] = 'down'
        })
        window.addEventListener('keyup', event =>{
            self.keydowns[event.key] = 'up'
        })
        this.init()
    }
    static instance(...args) {
        this.i = this.i || new this(...args)
        return this.i
    }
    drawImage(o){
        this.ctx.drawImage(o.texture, o.x, o.y)
    }
    update () {
        this.scene.update()
    }
    draw () {
        this.scene.draw()
    }
    //注册按键功能
    registerAction(key, callback) {
        this.actions[key] = callback
    }
    //游戏运行动作
    run_loop () {
        var g = this
        this.keys = Object.keys(this.actions)
        for (var i = 0; i < this.keys.length; i++) {
            var key = this.keys[i]
            var status = this.keydowns[key]
            if (status == 'down') {
                this.actions[key]('down')
            }
            else if (status == 'up') {
                this.actions[key]('up')
                g.keydowns[key] = null
            }
        }
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.update()
        this.draw()

        setTimeout(function () {
            g.run_loop()
        }, 1000/global_args.fps)
    }

    runWithScene (scene) {
        var g = this
        this.scene = scene
        //游戏运行
        setTimeout(function () {
            g.run_loop()
        }, 1000/global_args.fps)
    }
    // 载入其他场景
    replaceScene  (scene) {
        this.scene = scene
    }
    // start
    __start() {
        this.runCallback(this)
    }
    // 载入完成后开始
    init () {
        var g = this
        var img_loaded = 0
        var names = Object.keys(this.imgs_path)
        for (var i = 0; i < names.length; i++) {
            let name = names[i]
            let img = new Image
            img.src = this.imgs_path[name]
            img.onload = function () {
                global_args.images[name] = img
                img_loaded += 1
                if (img_loaded === names.length) {
                    g.__start()
                }
            }
        }
    }
}
