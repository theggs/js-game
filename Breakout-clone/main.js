// 载入关卡功能
var blocksFromLevel = function () {
    var blocks = []
    for (var i = 0; i < level.length; i++) {
        var block = Block(level[i])
        blocks.push(block)
    }
    return blocks
}


var _main = function () {
    var imgs_path = {
        ball: 'img/ball.png',
        block: 'img/block.png',
        paddle: 'img/paddle.png',
    }
    var game = Game.instance(imgs_path, function (g) {
        var s = new coverScene(g)
        g.runWithScene(s)
    })
}

_main()
