var log = console.log.bind(console)
var e = function (args) {
    return document.querySelector(args)
}

// 一个点是否在矩形内
var pointInRectangle = function (px, py, rectangle) { // rectangle 应该有 x, x1, y, y1 四个key
    var aInb = function (p, p1, p2) {
        return p >= p1 && p <= p2
    }
    return aInb(px, rectangle.x, rectangle.x + rectangle.w)
        && aInb(py, rectangle.y, rectangle.y + rectangle.h)
}
// 两个矩形是否相交
var collide = function (a, b) {
    a.x1 = a.x + a.w
    a.y1 = a.y + a.h
    b.x1 = b.x + b.w
    b.y1 = b.y + b.h
    // 矩形aa的一个角在矩形bb里
    var cornerInRectangle = function (aa, bb) {
        return pointInRectangle(aa.x, aa.y, bb)
            || pointInRectangle(aa.x, aa.y1, bb)
            || pointInRectangle(aa.x1, aa.y, bb)
            || pointInRectangle(aa.x1, aa.y1, bb)
    }
    return cornerInRectangle(a, b)
        || cornerInRectangle(b, a)

}
// 相交时方向
var collide_onside = function (a, b) {
    a.x1 = a.x + a.w
    a.y1 = a.y + a.h
    b.x1 = b.x + b.w
    b.y1 = b.y + b.h
    // 上一次刷新 a 是否在 b Y 方向外侧
    var historyANotInBY = function (aa, bb) {
        return aa.historyY > bb.y1 || aa.historyY1 < bb.y
    }
    // 上一次刷新 a 是否在 b X 方向外侧
    var historyANotInBX = function (aa, bb) {
        return aa.historyX > bb.x1 || aa.historyX1 < bb.x
    }

    var o = {
        x: false,
        y: false,
    }
    // 如果相交，根据上次刷新 a 的位置判断碰撞方向
    if (collide(a, b))  {
        if (historyANotInBX(a, b)) {
            o.x = true
        }
        if (historyANotInBY(a, b)) {
            o.y = true
        }
    }
    return o
}
var imgFromName = function (name) {
    return global_args.images[name]
}

var refreshElementById = function(e_id){
    var old_element = document.getElementById(e_id)
    var new_element = old_element.cloneNode(true)
    old_element.parentNode.replaceChild(new_element, old_element)
}
