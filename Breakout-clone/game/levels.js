var level= []

var pointBlockInBlocks = function (x, y) {
    for (var i = 0; i < level.length; i++) {
        let block = Block(level[i])
        if (collide(Block([x, y]), block)) {
            var in_it = true
            break
        }
    }
    return in_it || false
}

var addBlock = function (x, y) {
    if (!pointBlockInBlocks(x, y)) {
        level.push([x, y])
    }
}

var removeBlock = function (x, y) {
    for (var i = 0; i < level.length; i++) {
        let block = Block(level[i])
        if (pointInRectangle(x, y, block)) {
            level.splice(i, 1)
            break
        }
    }
}