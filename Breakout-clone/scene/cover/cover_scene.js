class coverScene extends Scene {
    constructor(game) {
        super(game)
        game.registerAction('k', function(){
            game.actions = {}
            var s = new mainScene(game)
            game.replaceScene(s)
        })
        game.registerAction('e', function(){
            game.actions = {}
            var s = new levelEditScene(game)
            game.replaceScene(s)
        })
    }
    draw() {
        // draw labels
        this.game.ctx.fillText('按 k 开始游戏', 100, 190)
        this.game.ctx.fillText('按 e 编辑关卡', 100, 150)
    }
}
