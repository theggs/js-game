var Block = function(p) {
    var img= imgFromName('block')
    var o = {
        img: img,
        x: p[0],
        y: p[1],
        alive: true,
        lives: p[2] || 1,
    }
    o.w = o.img.width
    o.h = o.img.height
    o.kill = function () {
        o.lives --
        if (o.lives < 1) {
            o.alive = false
        }
    }
    return o
}
