class mainScene extends Scene {
    constructor(game) {
        super(game)

        this.levelNum = localStorage.level || level

        this.paddle = Paddle()
        this.ball = Ball()
        this.blocks = blocksFromLevel(this.levelNum)

        global_args.score = 0
        //死亡线
        this.deadline = game.canvas.height

        this.debugOn = true

        // 注册按键事件
        game.registerAction('a', () => {
            this.paddle.moveLeft()
        })
        game.registerAction('d', () => {
            this.paddle.moveRight()
        })
        game.registerAction('f', () => {
        this.ball.fire()
    })

        this.debugMode = enabled => {
            if (!enabled) {
                return
            }
            log('Debug Mode On')
            // 暂停
            window.addEventListener('keydown', function (event) {
                if (event.key === 'p') {
                    this.ball.fired = !this.ball.fired
                }
            })
            // fps 控制 球速控制 无敌模式
            var insert_fps_contrler = () => {
                if (e('#debug-area')) {
                    e('#debug-area').parentElement.removeChild(e('#debug-area'))
                }
                var content = `
                <div id="debug-area">
                    <div><span>帧率控制：</span><input id="fps-contrler" type="range" max="60" min="1" value="60"></div>
                    <div><span>球速控制：</span><input id="ball-speed-contrler" type="range" max="15" min="1" value="10"></div>
                    <div><span>无敌模式：</span><input id="invincible-mode" type="checkbox"></div>
                </div>`
                e('#game-area').insertAdjacentHTML('afterend', content)
                e('#fps-contrler').addEventListener('input', event => {
                    global_args.fps = Number(event.target.value)
                })
                e('#ball-speed-contrler').addEventListener('input', event => {
                    var speed = Number(event.target.value)
                    this.ball.speedX = this.ball.speedX / Math.abs(this.ball.speedX) * speed
                    this.ball.speedY = this.ball.speedY / Math.abs(this.ball.speedY) * speed
                })
                e('#invincible-mode').addEventListener('change', event => {
                    if (event.target.checked) {
                        this.deadline = 1000
                        log(this.deadline)
                    }
                    else {
                        this.deadline = game.canvas.height
                        log(this.deadline)
                    }
                })
            }
            insert_fps_contrler()
            // 拖拽球
            var ball_draged = false
            this.game.canvas.addEventListener('mousedown', function (event) {
                if (this.ball) {
                    if (pointInRectangle(event.offsetX, event.offsetY, this.ball)) {
                        ball_draged = true
                    }
                }
            })

            this.game.canvas.addEventListener('mousemove', function (event) {
                if (ball_draged) {
                    this.ball.x = event.offsetX - this.ball.w / 2
                    this.ball.y = event.offsetY - this.ball.h / 2
                }
            })
            this.game.canvas.addEventListener('mouseup', function () {
                ball_draged = false
            })
        }
        this.debugMode(this.debugOn)
    }

    update () {
        this.ball.move()
        // 判断碰撞
        this.ball.actionWith(this.paddle, function (b) {})
        for (var i =0; i < this.blocks.length; i++) {
            if (this.blocks[i].alive) {
                this.ball.actionWith(this.blocks[i], function (block) {
                    block.kill()
                    global_args.score += 100
                    log(level)
                })
            }
        }
        //游戏结束
        if (this.ball.y + this.ball.h >= this.deadline) {
            log(this.ball.y + this.ball.h, this.deadline)
            // 清除事件
            this.game.actions = {}
            //载入新场景
            var end = new gameOverScene(this.game)
            this.game.replaceScene(end)
        }
        if (this.blocks.length === 0) {
            this.game.actions = {}
            var s = new sucessScene(this.game)
            this.game.replaceScene(s)
        }
    }
    draw() {
        // draw 背景
        // game.ctx.fillStyle = "#554"
        // game.ctx.fillRect(0, 0, 400, 300)
        // draw object
        this.game.drawImage(this.paddle)
        this.game.drawImage(this.ball)
        for (var i = 0; i < this.blocks.length; i++) {
            if (this.blocks[i].alive === true) {
                this.game.drawImage(this.blocks[i])
            }
        }
        //draw_score
        this.game.ctx.fillText('score: ' + global_args.score, 10, 290)
    }


}


// var mainScene = function (game) {
//     var levelNum = localStorage.level || level
//
//     var s = {
//         game: game,
//     }
//
//     var paddle = Paddle()
//     var ball = Ball()
//     var blocks = blocksFromLevel(levelNum)
//
//     var score = 0
//     //死亡线
//     var deadline = game.canvas.height
//
//
//     // 画面每次刷新内容
//     s.update = function () {
//         ball.move()
//         // 判断碰撞
//         ball.actionWith(paddle, function (b) {})
//         for (var i =0; i < blocks.length; i++) {
//             if (blocks[i].alive) {
//                 ball.actionWith(blocks[i], function (block) {
//                     block.kill()
//                     score += 100
//                     log(level)
//                 })
//             }
//         }
//         //游戏结束
//         if (ball.y + ball.h >= deadline) {
//             // 清除事件
//             game.actions = {}
//             //载入新场景
//             var end = new gameOverScene(game)
//             game.replaceScene(end)
//         }
//         if (blocks.length === 0) {
//             game.actions = {}
//             var s = new sucessScene(game)
//             game.replaceScene(s)
//         }
//     }
//
//     // 画画面
//     s.draw = function () {
//         // draw 背景
//         // game.ctx.fillStyle = "#554"
//         // game.ctx.fillRect(0, 0, 400, 300)
//         // draw object
//         game.drawImage(paddle)
//         game.drawImage(ball)
//         for (var i = 0; i < blocks.length; i++) {
//             if (blocks[i].alive === true) {
//                 game.drawImage(blocks[i])
//             }
//         }
//         //draw_score
//         game.ctx.fillText('score: ' + score, 10, 290)
//
//     }
//     // 注册按键事件
//     game.registerAction('a', function () {
//         paddle.moveLeft()
//     })
//     game.registerAction('d', function () {
//         paddle.moveRight()
//     })
//     game.registerAction('f', function () {
//         ball.fire()
//     })
//     // debug 功能
//     var debugMode = function(enabled) {
//         if (!enabled) {
//             return
//         }
//         log('Debug Mode On')
//         // 暂停
//         window.addEventListener('keydown', function (event) {
//             if (event.key === 'p') {
//                 ball.fired = !ball.fired
//             }
//         })
//         // fps 控制 和 球速控制
//         var insert_fps_contrler = function () {
//             if (e('#debug-area')) {
//                 e('#debug-area').parentElement.removeChild(e('#debug-area'))
//             }
//             var content = `
//             <div id="debug-area">
//                 <div><span>帧率控制：</span><input id="fps-contrler" type="range" max="60" min="1" value="60"></div>
//                 <div><span>球速控制：</span><input id="ball-speed-contrler" type="range" max="15" min="1" value="10"></div>
//                 <div><span>无敌模式：</span><input id="invincible-mode" type="checkbox"></div>
//             </div>`
//             e('#game-area').insertAdjacentHTML('afterend', content)
//             e('#fps-contrler').addEventListener('input', function (event) {
//                 global_args.fps = Number(event.target.value)
//             })
//             e('#ball-speed-contrler').addEventListener('input', function (event) {
//                 var speed = Number(event.target.value)
//                 ball.speedX = ball.speedX / Math.abs(ball.speedX) * speed
//                 ball.speedY = ball.speedY / Math.abs(ball.speedY) * speed
//             })
//             e('#invincible-mode').addEventListener('change', function (event) {
//                 if (event.target.checked) {
//                     deadline = 1000
//                 }
//                 else {
//                     deadline = game.canvas.height
//                 }
//             })
//         }
//         insert_fps_contrler()
//         // 拖拽球
//         var ball_draged = false
//         game.canvas.addEventListener('mousedown', function (event) {
//             if (pointInRectangle(event.offsetX, event.offsetY, ball)) {
//                 ball_draged = true
//             }
//         })
//         game.canvas.addEventListener('mousemove', function (event) {
//             log(game.keydowns)
//             if (ball_draged) {
//                 ball.x = event.offsetX - ball.w / 2
//                 ball.y = event.offsetY - ball.h / 2
//             }
//         })
//         game.canvas.addEventListener('mouseup', function () {
//             ball_draged = false
//         })
//     }
//     debugMode(true)
//
//     return s
// }