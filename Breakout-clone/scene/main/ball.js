var Ball = function() {
    var o = {
        img: imgFromName('ball'),
        x: 200,
        y: 230,
        speedX: 5,
        speedY: -5,
        fired: false,
    }
    o.w = o.img.width
    o.h = o.img.height
    o.fire = function () {
        o.fired = true
    }
    o.move = function () {
        if (o.fired) {
            if (o.x <= 0 || o.x >= 400 - o.img.width) {
                o.speedX = -o.speedX
            }
            if (o.y <= 0 || o.y >= 300 - o.img.height) {
                o.speedY = -o.speedY
            }
            // move
            o.historyX = o.x
            o.historyX1 = o.x + o.w
            o.historyY = o.y
            o.historyY1 = o.y + o.h
            o.x += o.speedX
            o.y += o.speedY
        }
    }
    o.collide_onside = function (targ) {
        return collide_onside(o, targ)
    }
    o.actionWith = function (targ, callback) {
        if (o.collide_onside(targ).x) {
            //X方向碰撞
            o.speedX *= -1
        }
        if (o.collide_onside(targ).y) {
            //Y方向碰撞
            o.speedY *= -1
        }
        if (o.collide_onside(targ).x || o.collide_onside(targ).y) {
            callback(targ)
        }
    }
    return o
}
