var Paddle = function() {
    var o = {
        img: imgFromName('paddle'),
        x: 100,
        y: 250,
        speed: 5,
    }
    o.w = o.img.width
    o.h = o.img.height
    o.moveLeft = function () {
        if (o.x > 0) {
            o.x -= o.speed
        }
    }
    o.moveRight = function () {
        if (o.x < 400 - o.img.width) {
            o.x += o.speed
        }
    }
    return o
}
