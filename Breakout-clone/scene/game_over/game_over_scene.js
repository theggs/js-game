class gameOverScene extends Scene {
    constructor(game) {
        super(game)
        game.registerAction('r', function(){
            game.actions = {}
            var s = new coverScene(game)
            game.replaceScene(s)
        })
    }
    draw() {
        // draw labels
        this.game.ctx.fillText('游戏结束, 按 r 返回标题界面', 100, 290)
    }
}
