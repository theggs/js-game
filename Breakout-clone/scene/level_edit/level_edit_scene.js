class levelEditScene extends Scene {
    constructor(game) {
        super(game)
        this.game = game
        game.registerAction('s', function(){
            localStorage.level = level
            game.actions = {}
            var s = new coverScene(game)
            game.replaceScene(s)
        })

        // 增加/删除方块
        this.block_add = false
        this.mouse_down = false
        game.canvas.addEventListener('mousedown', function (event) {
            this.mouse_down = true
            if (!pointBlockInBlocks(event.offsetX, event.offsetY)) {
                this.block_add = true
                addBlock(event.offsetX, event.offsetY)
            }
        })
        game.canvas.addEventListener('mousemove', function (event) {
            if (this.mouse_down) {
                log('block_add', this.block_add)
                if (this.block_add) {
                    addBlock(event.offsetX, event.offsetY)
                }
                else {
                    removeBlock(event.offsetX, event.offsetY)
                }
            }
        })
        game.canvas.addEventListener('mouseup', function () {
            this.block_add = false
            this.mouse_down = false
        })
    }
    draw() {
        // draw labels
        this.game.ctx.fillText('按 s 保存关卡并返回', 100, 190)
        var blocks = blocksFromLevel()
        for (var i = 0; i < blocks.length; i++) {
            if (blocks[i].alive === true) {
                this.game.drawImage(blocks[i])
            }
        }
    }

}
