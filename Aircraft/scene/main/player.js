class Player extends Element {
    constructor (game) {
        super(game, 'player')

        this.setup()
    }

    setup() {
        var game = this.game

        this.x = (game.canvas.width - this.w) / 2
        this.y = 500
        this.speed = 10
        this.cooldown = 0

        this.life = 50

    }

    update() {
        // golden finger
        if (this.cooldown > 0) {
            this.cooldown --
        }


        for (e of this.scene.enemies) {
            this.actionWithEnemy(e)
        }
    }
    debug() {
        this.speed = config.player_speed
    }
    fire() {
        if (this.cooldown == 0) {
            this.cooldown = config.fire_cooldown
            var b = new Bullet(this.game)
            var bx = this.x + this.w / 2 - b.w / 2
            var by = this.y
            b.x = bx
            b.y = by
            this.scene.addElement(b)
        }
    }

    actionWithEnemy(e) {
        if (collide(this, e)) {
            e.arm -= 1
            this.life -= 1
            global_args.score += 100
        }
    }
    moveLeft (){
        if (this.x > -this.w/2) {
            this.x -= this.speed
        }
    }
    moveRight (){
        if (this.x < this.game.canvas.width - this.w / 2) {
            this.x += this.speed
        }
    }
    moveUp (){
        if (this.y > - this.h / 2) {
            this.y -= this.speed
        }
    }
    moveDown (){
        if (this.y < this.game.canvas.height - this.h / 2 ) {
            this.y += this.speed
        }
    }

}