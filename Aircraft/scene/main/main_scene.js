class MainScene extends Scene {
    constructor(game) {
        super(game)


        this.setup()
        this.setupInputs()
    }
    setup() {
        var game = this.game
        this.numberOfEnemies = 6
        this.bg = new Backgound(game)
        this.player = new Player(game)

        global_args.score = 0

        this.addElement(this.bg)
        this.addElement(this.player)

        this.addEnemies()

        this.enemies_down_framenum = {
            0: 4,
            1: 4,
            2: 6,
        }
    }
    addEnemies() {
        var es = []
        for (var i = 0; i < this.numberOfEnemies; i++) {
            var e = new Enemy(this.game)
            es.push(e)
            this.addElement(e)
        }
        this.enemies = es
    }
    setupInputs() {
        var game = this.game
        // 注册按键事件
        game.registerAction('a', () => {
            this.player.moveLeft()
        })
        game.registerAction('d', () => {
            this.player.moveRight()
        })
        game.registerAction('w', () => {
            this.player.moveUp()
        })
        game.registerAction('s', () => {
            this.player.moveDown()
        })
        game.registerAction('j', () => {
            this.player.fire()
        })    }
    update() {
        super.update()
        if (this.player.life <= 0) {
            this.game.actions = {}
            var s = new GameOverScene(this.game)
            this.game.replaceScene(s)
        }
    }
    draw() {
        super.draw()
        this.game.ctx.fillText(`分数: ${global_args.score}`, 25, 25)
        this.game.ctx.fillText(`剩余生命: ${this.player.life}`, 25, 50)

    }
}

//     constructor(game) {
//         super(game)
//
//         this.levelNum = localStorage.level || level
//
//         this.paddle = Paddle()
//         this.ball = Ball()
//         this.blocks = blocksFromLevel(this.levelNum)
//
//         global_args.score = 0
//         //死亡线
//         this.deadline = game.canvas.height
//
//         this.debugOn = true
//
//         // 注册按键事件
//         game.registerAction('a', () => {
//             this.paddle.moveLeft()
//         })
//         game.registerAction('d', () => {
//             this.paddle.moveRight()
//         })
//         game.registerAction('f', () => {
//         this.ball.fire()
//     })
//
//         this.debugMode = enabled => {
//             if (!enabled) {
//                 return
//             }
//             log('Debug Mode On')
//             // 暂停
//             window.addEventListener('keydown', function (event) {
//                 if (event.key === 'p') {
//                     this.ball.fired = !this.ball.fired
//                 }
//             })
//             // fps 控制 球速控制 无敌模式
//             var insert_fps_contrler = function () {
//                 if (e('#debug-area')) {
//                     e('#debug-area').parentElement.removeChild(e('#debug-area'))
//                 }
//                 var content = `
//                 <div id="debug-area">
//                     <div><span>帧率控制：</span><input id="fps-contrler" type="range" max="60" min="1" value="60"></div>
//                     <div><span>球速控制：</span><input id="ball-speed-contrler" type="range" max="15" min="1" value="10"></div>
//                     <div><span>无敌模式：</span><input id="invincible-mode" type="checkbox"></div>
//                 </div>`
//                 e('#game-area').insertAdjacentHTML('afterend', content)
//                 e('#fps-contrler').addEventListener('input', event => {
//                     global_args.fps = Number(event.target.value)
//                 })
//                 e('#ball-speed-contrler').addEventListener('input', event => {
//                     var speed = Number(event.target.value)
//                     this.ball.speedX = this.ball.speedX / Math.abs(this.ball.speedX) * speed
//                     this.ball.speedY = this.ball.speedY / Math.abs(this.ball.speedY) * speed
//                 })
//                 e('#invincible-mode').addEventListener('change', event => {
//                     if (event.target.checked) {
//                         this.deadline = 1000
//                         log(this.deadline)
//                     }
//                     else {
//                         this.deadline = game.canvas.height
//                         log(this.deadline)
//                     }
//                 })
//             }
//             insert_fps_contrler()
//             // 拖拽球
//             var ball_draged = false
//             this.game.canvas.addEventListener('mousedown', function (event) {
//                 if (this.ball) {
//                     if (pointInRectangle(event.offsetX, event.offsetY, this.ball)) {
//                         ball_draged = true
//                     }
//                 }
//             })
//
//             this.game.canvas.addEventListener('mousemove', function (event) {
//                 if (ball_draged) {
//                     this.ball.x = event.offsetX - this.ball.w / 2
//                     this.ball.y = event.offsetY - this.ball.h / 2
//                 }
//             })
//             this.game.canvas.addEventListener('mouseup', function () {
//                 ball_draged = false
//             })
//         }
//         this.debugMode(this.debugOn)
//     }
//
//     update () {
//         this.ball.move()
//         // 判断碰撞
//         this.ball.actionWith(this.paddle, function (b) {})
//         for (var i =0; i < this.blocks.length; i++) {
//             if (this.blocks[i].alive) {
//                 this.ball.actionWith(this.blocks[i], function (block) {
//                     block.kill()
//                     global_args.score += 100
//                     log(level)
//                 })
//             }
//         }
//         //游戏结束
//         if (this.ball.y + this.ball.h >= this.deadline) {
//             log(this.ball.y + this.ball.h, this.deadline)
//             // 清除事件
//             this.game.actions = {}
//             //载入新场景
//             var end = new GameOverScene(this.game)
//             this.game.replaceScene(end)
//         }
//         if (this.blocks.length === 0) {
//             this.game.actions = {}
//             var s = new sucessScene(this.game)
//             this.game.replaceScene(s)
//         }
//     }
//     draw() {
//         // draw 背景
//         // game.ctx.fillStyle = "#554"
//         // game.ctx.fillRect(0, 0, 400, 300)
//         // draw object
//         this.game.drawImage(this.paddle)
//         this.game.drawImage(this.ball)
//         for (var i = 0; i < this.blocks.length; i++) {
//             if (this.blocks[i].alive === true) {
//                 this.game.drawImage(this.blocks[i])
//             }
//         }
//         //draw_score
//         this.game.ctx.fillText('score: ' + global_args.score, 10, 290)
//     }
//
//
// }
//
