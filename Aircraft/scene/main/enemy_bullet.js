class EnemyBullet extends Element {
    constructor (game) {
        super(game, 'enemy_bullet')

        this.setup()
    }

    setup() {
        this.speed = 5
    }

    update() {
        this.y += this.speed
        if (this.x > this.game.canvas.height) {
            this.life = 0
        }
        this.actionWithPlayer(this.scene.player)
    }
    actionWithPlayer(p) {
        if (collide(this, p)) {
            this.life -= 1
            p.life -= 1

            var boom = new particleSystem(this.game, this.x + this.w / 2, this.y + this.h / 2, 2)
            this.scene.elements.push(boom)

        }
    }
}