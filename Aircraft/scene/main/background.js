class Backgound extends Element {
    constructor (game) {
        super(game, 'background')

        this.setup()
    }

    setup() {
        this.x = 0
        this.y = -this.texture.height / 2
        this.speed = config.background_speed
    }

    update() {
        this.y += this.speed
        if (this.y >= 0) {
            this.y = -this.texture.height / 2
        }
    }
    debug() {
        this.speed = config.background_speed
    }
}