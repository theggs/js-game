class GameOverScene extends Scene {
    constructor(game) {
        super(game)
        game.registerAction('r', function(){
            game.actions = {}
            var s = new CoverScene(game)
            game.replaceScene(s)
        })
    }
    draw() {
        // draw labels
        this.game.ctx.fillText('游戏结束, 按 r 返回标题界面', 150, 300)
        this.game.ctx.fillText(`分数：${global_args.score}`, 150, 350)
    }
}
