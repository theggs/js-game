class Particle extends Element {
    constructor (game) {
        super(game, 'particle')

        this.setup()
    }

    setup() {
        this.v_factor = 0.1
    }
    init(x, y, vx, vy) {
        this.x = x
        this.y = y
        this.vx = vx
        this.vy = vy
    }
    update() {
        this.x += this.vx
        this.y += this.vy
        this.vx -= this.v_factor * this.vx
        this.vy -= this.v_factor * this.vy
    }
    debug() {
    }
}

class particleSystem {
    constructor(game, x, y, particles_number) {
        this.game = game
        this.setup()

        this.x = x
        this.y = y
        this.life = 1
        this.particles_number = particles_number
    }
    setup() {
        this.particles = []
    }
    update() {
        //添加火花
        if (this.particles.length < this.particles_number) {
            var p = new Particle(this.game)
            // 设置初始化
            var vx = randombetween(-10, 10)
            var vy = randombetween(-10, 10)
            p.init(this.x, this.y, vx, vy)
            this.particles.push(p)
        }
        //更新所有火花
        for (var p of this.particles) {
            p.update()
        }
        // 小火花全部停止，死掉
        var il = this.particles.filter(p => Math.abs(p.vx) > 0.001 & Math.abs(p.vy) > 0.001)
        if (il.length == 0) {
            this.life = 0
        }
    }
    draw() {
        for (var p of this.particles) {
            p.draw()
        }
    }
}