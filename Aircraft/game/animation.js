class Animation {
    constructor(game, frame_num, frame_name) {
        this.game = game
        // hard code
        this.frames = []
        for (var i = 1; i < frame_num + 1; i++) {
            var name = frame_name + String(i)
            var t = imgFromName(name)
            this.frames.push(t)
        }
        this.texture = this.frames[0]
        this.frameIndex = 0
        this.frameCount = 5

        this.x = 0
        this.y = 0

        this.life = 1

    }

    update() {
        this.frameCount--
        if (this.frameCount == 0) {
            this.frameIndex = (this.frameIndex + 1) % this.frames.length
            this.texture = this.frames[this.frameIndex]
            this.frameCount = 5
        }
    }
    draw(){
        this.game.drawImage(this)
    }
}

class AnimationCanDie extends Animation{
    constructor(game, frame_num, frame_name) {
        super(game, frame_num, frame_name)
        this.life = this.frameCount
    }
    update() {
        super.update()
        if (this.frameIndex == 0) {
            this.life -= 1
        }
    }
}

// 多态动画 demo
class AnimationMutiCon {
    constructor(game) {
        this.game = game
        // hard code
        this.animations = {
            idle: [],
            run: [],
        }
        for (var i = 1; i <  5; i++) {
            var name = `enemy0_down${i}`
            var t = imgFromName(name)
            this.animations.idle.push(t)
        }
        for (var i = 1; i <  5; i++) {
            var name = `enemy1_down${i}`
            var t = imgFromName(name)
            this.animations.run.push(t)
        }
        this.animationName = 'idle'
        this.texture = this.frames()[0]
        this.frameIndex = 0
        this.frameCount = 5

        this.x = 0
        this.y = 0
        this.flipX = false

        this.life = 1
    }
    frames() {
        return this.animations[this.animationName]
    }
    update() {
        this.frameCount--
        if (this.frameCount == 0) {
            this.frameIndex = (this.frameIndex + 1) % this.frames().length
            this.texture = this.frames()[this.frameIndex]
            this.frameCount = 5
            this.w = this.texture.width
            this.h = this.texture.height
        }
    }
    draw(){
        var ctx = this.game.ctx
        if (this.flipX) {
            ctx.save()

            var x = this.x + this.w / 2
            var y = this.y
            ctx.translate(x, y)
            ctx.scale(-1, 1)
            ctx.translate(-x, y)

            ctx.drawImage(this.texture, 0, 0)

            ctx.restore()
        }
        else {
            ctx.drawImage(this.texture, this.x, this.y)
        }
    }
    move(x, keyStatus) {
        this.flipX = x < 0
        this.x += x
        var animationNames = {
            down: 'run',
            up: 'idle',
        }
        var name = animationNames[keyStatus]
        this.changeAnimation(name)
    }
    changeAnimation(name) {
        this.animationName = name
    }
}
