// global_args 只准在 main 和 utils 中使用

const global_args = {
    images: {},
    fps: 60,
    score: 0,
}
const config = {
    debug: true,
    player_speed: 10,
    bullet_speed: 25,
    enemy_bullet_speed: 10,
    fire_cooldown: 8,
    background_speed: 1,
    enemy_cooldown: 120,
}