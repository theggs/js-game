class Game {
    constructor(imgs_path, runCallback) {
        this.imgs_path = imgs_path
        this.runCallback = runCallback
        this.keydowns =  {}
        this.actions = {}
        this.scene =  null

        this.canvas = e('#game-area')
        this.ctx = this.canvas.getContext('2d')
        var self = this
        // 监听按键
        window.addEventListener('keydown', event =>{
            this.keydowns[event.key] = 'down'
        })
        window.addEventListener('keyup', event =>{
            self.keydowns[event.key] = 'up'
        })
        this.init()
    }
    static instance(...args) {
        this.i = this.i || new this(...args)
        return this.i
    }
    drawImage(o){
        this.ctx.drawImage(o.texture, o.x, o.y)
    }
    update () {
        this.scene.update()
    }
    draw () {
        this.scene.draw()
    }
    //注册按键功能
    registerAction(key, callback) {
        this.actions[key] = callback
    }
    //游戏运行动作
    run_loop () {
        var g = this
        this.keys = Object.keys(this.actions)
        for (var i = 0; i < this.keys.length; i++) {
            var key = this.keys[i]
            var status = this.keydowns[key]
            if (status == 'down') {
                this.actions[key]('down')
            }
            else if (status == 'up') {
                this.actions[key]('up')
                g.keydowns[key] = null
            }
        }
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.update()
        this.draw()

        setTimeout(function () {
            g.run_loop()
        }, 1000/global_args.fps)
    }

    runWithScene (scene) {
        var g = this
        this.scene = scene
        //游戏运行
        setTimeout(function () {
            g.run_loop()
        }, 1000/global_args.fps)
    }
    // 载入其他场景
    replaceScene  (scene) {
        this.scene = scene
    }
    // start
    __start() {
        this.runCallback(this)
    }
    // 载入完成后开始
    init () {
        var g = this
        var img_loaded = 0
        var names = Object.keys(this.imgs_path)
        for (var i = 0; i < names.length; i++) {
            let name = names[i]
            let img = new Image
            img.src = this.imgs_path[name]
            img.onload = function () {
                global_args.images[name] = img
                img_loaded += 1
                if (img_loaded === names.length) {
                    g.__start()
                }
            }
        }
    }
}




//
//
// var Game = function (imgs_path, runCallback) {
//     var g = {
//         drawlist: [],
//         keydowns: {},
//         actions: {},
//         scene: null,
//     }
//     g.canvas = e('#game-area')
//     g.ctx = g.canvas.getContext('2d')
//     g.drawImage = function (o) {
//         g.ctx.drawImage(o.img, o.x, o.y)
//     }
//
//     g.update = function () {
//         g.scene.update()
//     }
//     g.draw = function () {
//         g.scene.draw()
//     }
//
//     // 监听按键
//     window.addEventListener('keydown', function (event) {
//         g.keydowns[event.key] = true
//     })
//     window.addEventListener('keyup', function (event) {
//         g.keydowns[event.key] = false
//     })
//
//     //注册按键功能
//     g.registerAction = function (key, callback) {
//         g.actions[key] = callback
//     }
//
//     //游戏运行动作
//     var run_loop = function () {
//         g.keys = Object.keys(g.actions)
//         for (var i = 0; i < g.keys.length; i++) {
//             var key = g.keys[i]
//             if (g.keydowns[key]) {
//                 g.actions[key]()
//             }
//         }
//         g.ctx.clearRect(0, 0, g.canvas.width, g.canvas.height)
//         g.update()
//         g.draw()
//
//         setTimeout(function () {
//             run_loop()
//         }, 1000/global_args.fps)
//     }
//
//     var img_loaded = 0
//     var names = Object.keys(imgs_path)
//     for (var i=0; i<names.length; i++) {
//         let name = names[i]
//         let img = new Image
//         img.src = imgs_path[name]
//         img.onload = function () {
//             global_args.images[name] = img
//             img_loaded += 1
//             if (img_loaded === names.length) {
//                 g.__start()
//             }
//         }
//     }
//     g.__start = function (scene) {
//         runCallback(g)
//     }
//
//     g.runWithScene = function (scene) {
//         g.scene = scene
//         //游戏运行
//         setTimeout(function () {
//             run_loop()
//         }, 1000/global_args.fps)
//     }
//
//     // 载入其他场景
//     g.replaceScene = function (scene) {
//         g.scene = scene
//     }
//
//     return g
// }
