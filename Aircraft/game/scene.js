class Scene {
    constructor(game) {
        this.game = game
        this.elements = []

        this.cleanup()
    }
    cleanup() {
        this.game.actions = {}
        this.game.keydowns = {}
    }
    draw(){
        for (var e of this.elements) {
            // this.game.drawImage(e)
            e.draw()
        }
    }
    update(){
        for (var i = 0; i < this.elements.length; i++) {
            var e = this.elements[i]
            e.update()
        }
        // 删除死掉的元素
        this.elements = this.elements.filter(e => e.life > 0)
        // debug
        if (config.debug) {
            for (var i = 0; i < this.elements.length; i++) {
                var e = this.elements[i]
                e.debug && e.debug()
            }
        }
    }
    addElement(element) {
        element.scene = this
        this.elements.push(element)
    }
}
