class Element {
    constructor(game, name) {
        this.game = game
        this.texture = imgFromName(name)

        this.x = 0
        this.y = 0
        this.w = this.texture.width
        this.h = this.texture.height

        this.life = 1
    }
    draw(){
        this.game.drawImage(this)
    }
    update(){
    }
}
