var _main = function () {
    var imgs_path = {
        background: 'img/background.png',
        bullet: 'img/bullet.png',
        enemy_bullet: 'img/enemy_bullet.png',
        player: 'img/player.png',
        enemy0: 'img/enemy0.png',
        enemy1: 'img/enemy1.png',
        enemy2: 'img/enemy2.png',
        particle: 'img/particle.png',

        enemy0_down1: 'img/enemy0_down1.png',
        enemy0_down2: 'img/enemy0_down2.png',
        enemy0_down3: 'img/enemy0_down3.png',
        enemy0_down4: 'img/enemy0_down4.png',
        enemy1_down1: 'img/enemy1_down1.png',
        enemy1_down2: 'img/enemy1_down2.png',
        enemy1_down3: 'img/enemy1_down3.png',
        enemy1_down4: 'img/enemy1_down4.png',
        enemy2_down1: 'img/enemy2_down1.png',
        enemy2_down2: 'img/enemy2_down2.png',
        enemy2_down3: 'img/enemy2_down3.png',
        enemy2_down4: 'img/enemy2_down4.png',
        enemy2_down5: 'img/enemy2_down5.png',
        enemy2_down6: 'img/enemy2_down6.png',

    }
    var game = Game.instance(imgs_path, function (g) {
        var s = new CoverScene(g)
        g.runWithScene(s)
    })
}

_main()
